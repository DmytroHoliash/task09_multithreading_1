package com.holiash.view;

import com.holiash.controller.Command;
import com.holiash.controller.FixedThreadExecutorFibonacci;
import com.holiash.controller.PingPong;
import com.holiash.controller.PipeConnection;
import com.holiash.controller.RandomTimeSleeper;
import com.holiash.controller.SumFibonacci;
import com.holiash.controller.SyncOne;
import com.holiash.controller.SyncThree;
import com.holiash.controller.ThreadFibonacci;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class View {

  private List<String> menu;
  private Map<Integer, Command> menuCommands;

  public View() {
    this.menu = new ArrayList<>();
    this.menuCommands = new HashMap<>();
    initMenu();
    initMenuMethods();
  }

  private void initMenu() {
    this.menu.add("1 - Ping-Pong");
    this.menu.add("2 - Fibonacci using Thread");
    this.menu.add("3 - Fibonacci using Executor");
    this.menu.add("4 - Fibonacci sum");
    this.menu.add("5 - Sleeper using ScheduleExecutorService");
    this.menu.add("6 - Three methods sync one object");
    this.menu.add("7 - Three methods sync three different objects");
    this.menu.add("8 - Pipe connection");
    this.menu.add("0 - Exit");
  }

  private void initMenuMethods() {
    menuCommands.put(1, new PingPong());
    menuCommands.put(2, new ThreadFibonacci());
    menuCommands.put(3, new FixedThreadExecutorFibonacci());
    menuCommands.put(4, new SumFibonacci());
    menuCommands.put(5, new RandomTimeSleeper());
    menuCommands.put(6, new SyncOne());
    menuCommands.put(7, new SyncThree());
    menuCommands.put(8, new PipeConnection());
  }

  public void printMenu() {
    this.menu.forEach(System.out::println);
  }

  public void run(int choice) {
    if (this.menuCommands.containsKey(choice)) {
      menuCommands.get(choice).execute();
    }
  }
}
