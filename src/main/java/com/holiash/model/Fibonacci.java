package com.holiash.model;

public class Fibonacci {

  public static long fibb(int n) {
    if (n <= 1) {
      return n;
    }
    return fibb(n - 1) + fibb(n - 2);
  }
}
