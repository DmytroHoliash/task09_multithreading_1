package com.holiash.controller;

public class SyncThree implements Command {

  private final Object sync1 = new Object();
  private final Object sync2 = new Object();
  private final Object sync3 = new Object();

  @Override
  public void execute() {
    new Thread(this::method2).start();
    new Thread(this::method1).start();
    new Thread(this::method3).start();
  }

  private void method1() {
    synchronized (sync1) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method1");
      }
    }
  }

  private void method2() {
    synchronized (sync2) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method2");
      }
    }
  }

  private void method3() {
    synchronized (sync3) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method3");
      }
    }
  }

}
