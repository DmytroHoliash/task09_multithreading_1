package com.holiash.controller;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeConnection implements Command {

  @Override
  public void execute() {
    PipedInputStream pipedInputStream = new PipedInputStream();
    PipedOutputStream pipedOutputStream = new PipedOutputStream();
    try {
      pipedInputStream.connect(pipedOutputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
    Thread out = new Thread(() -> {
      String text = "text";
      try {
        for (char c : text.toCharArray()) {
          pipedOutputStream.write(c);
          System.out.println("Wrote " + c);
        }
        pipedOutputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    Thread in = new Thread(() -> {
      try {
        int c;
        while ((c = pipedInputStream.read()) != -1) {
          System.out.println("Read " + (char) c);
        }
        pipedInputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    in.start();
    out.start();
    try {
      out.join();
      in.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
