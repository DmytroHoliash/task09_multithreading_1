package com.holiash.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PingPong implements Command {

  private static final Object sync = new Object();
  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  public void execute() {
    Thread t1 = new Thread(() -> {
      synchronized (sync) {
        try {
          while (true) {
            if (Thread.interrupted()) {
              break;
            } else {
              sync.wait();
              Thread.sleep(1000);
              System.out.println("ping");
              sync.notify();
            }
          }
        } catch (InterruptedException e) {
          System.out.println("T1 interrupted");
        }
      }
    });
    Thread t2 = new Thread(() -> {
      synchronized (sync) {
        try {
          while (true) {
            if (Thread.interrupted()) {
              break;
            } else {
              sync.notify();
              sync.wait();
              Thread.sleep(1000);
              System.out.println("pong");
            }
          }
        } catch (InterruptedException e) {
          System.out.println("T2 interrupted");
        }
      }
    });
    System.out.println("Input y to stop it: ");
    t1.start();
    t2.start();
    String choice = null;
    try {
      choice = br.readLine();
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (choice.equals("y")) {
      t1.interrupt();
      t2.interrupt();
    }
  }
}
