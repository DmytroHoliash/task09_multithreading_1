package com.holiash.controller;

public class SyncOne implements Command {

  private final int THREAD_NUM = 3;
  private final Object sync = new Object();

  @Override
  public void execute() {
    Thread[] threads = new Thread[THREAD_NUM];
    threads[1] = new Thread(this::method1);
    threads[0] = new Thread(this::method2);
    threads[2] = new Thread(this::method3);
    for (int i = 0; i < THREAD_NUM; i++) {
      threads[i].start();
    }
    for (int i = 0; i < THREAD_NUM; i++) {
      try {
        threads[i].join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void method1() {
    synchronized (sync) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method1");
      }
    }
  }

  private void method2() {
    synchronized (sync) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method2");
      }
    }
  }

  private void method3() {
    synchronized (sync) {
      for (int i = 0; i < 5; i++) {
        System.out.println("Method3");
      }
    }
  }
}
