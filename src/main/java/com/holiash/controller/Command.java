package com.holiash.controller;

public interface Command {
  void execute();
}
