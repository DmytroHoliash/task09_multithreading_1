package com.holiash.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RandomTimeSleeper implements Command {

  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  @Override
  public void execute() {
    System.out.println("Input quantity of tasks: ");
    int quantity = 0;
    try {
      quantity = Integer.parseInt(br.readLine());
    } catch (IOException e) {
      e.printStackTrace();
    }
    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    for (int i = 0; i < quantity; i++) {
      int seconds = (int) (Math.random() * 10 + 1);
      executor.schedule(() -> System.out.println(seconds), seconds, TimeUnit.SECONDS);
    }
    executor.shutdown();
  }
}
