package com.holiash.controller;

import com.holiash.model.Fibonacci;

public class ThreadFibonacci implements Command {

  @Override
  public void execute() {
    final int[] numbers = {5, 9, 20, 35, 7};
    for (int number : numbers) {
      new Thread(() -> System.out.println(Fibonacci.fibb(number))).start();
    }
  }
}
