package com.holiash.controller;

import com.holiash.model.Fibonacci;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadExecutorFibonacci implements Command {

  @Override
  public void execute() {
    final int[] numbers = {5, 9, 20, 35, 7};
    ExecutorService executor = Executors.newFixedThreadPool(numbers.length);
    for (int number : numbers) {
      executor.execute(() -> System.out.println(Fibonacci.fibb(number)));
    }
    executor.shutdown();
  }
}
