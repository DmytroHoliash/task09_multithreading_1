package com.holiash.controller;

import com.holiash.model.Fibonacci;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class SumFibonacci implements Command {

  @Override
  public void execute() {
    ExecutorService executor = Executors.newWorkStealingPool();
    try {
      long sum = executor.invokeAll(createCallables()).stream()
        .mapToLong(future -> {
          try {
            return future.get();
          } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
          }
          return 0;
        }).sum();
      executor.shutdown();
      System.out.println("Fibonacci sum = " + sum);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  private List<Callable<Long>> createCallables() {
    final int[] numbers = {5, 9, 20, 35, 7};
    return Arrays.stream(numbers)
      .mapToObj(number -> (Callable<Long>) () -> Fibonacci.fibb(number))
      .collect(Collectors.toList());
  }
}
